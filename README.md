# Spring Boot Docker Kubernetes



## Commands important for Docker
```
docker build -t usuarios . -f .\msvc-usuarios\Dockerfile

docker build -t cursos . -f .\msvc-cursos\Dockerfile 

docker build -t gateway . -f .\msvc-gateway\Dockerfile

docker run -d -p 3308:3306 --name mysql8  --network spring -e MYSQL_ROOT_PASSWORD=valenzuela21 -e MYSQL_DATABASE=msvc_usaurios -v data-mysql:/var/lib/mysql --restart=always mysql:8


docker run -d -p 5532:5432 --name postgres14  --network spring -e POSTGRES_PASSWORD=valenzuela21 -e POSTGRES_DB=msvc_cursos -v data-postgres:/var/lib/postgresql/data --restart=always postgres:14-alpine 

docker run -d -p 8001:8001 --rm --name msvc-usuarios --network spring usuarios

docker run -d -p 8002:8002 --rm --name msvc-cursos --network spring cursos

docker run -d -p 8001:8001 --env-file .\msvc-usuarios\.env --rm --name msvc-usuarios --network spring usuarios

docker run -d -p 8002:8002 --env-file .\msvc-cursos\.env --rm --name msvc-cursos --network spring cursos

```

## Command Kubectl

```
kubectl get pods 
kubetcl get all
kubectl get services
kubectl get network
kubectl get volumens
kubectl delete -f  name.yaml
kubectl apply -f name.yaml
```

## Minikube Url

```
minikube service msvc-gateway --url

```



